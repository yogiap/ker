package com.kominfo.malang.smartcity.berita;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.kominfo.malang.smartcity.R;

/**
 * Created by USER on 20/02/2018.
 */

public class MainWeb extends Activity {
    String url_adapter_x;
    String hostname_adapter, spec;
    private WebView view;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview);

        Intent cuy = getIntent();
        url_adapter_x = cuy.getStringExtra("link_detail");
        hostname_adapter = cuy.getStringExtra("hostname");
        spec = "https://" + hostname_adapter + "/";

        Log.e("hostname_adapter", "onCreate: " + spec);
        Log.e("hostname_adapter", "onCreate: " + url_adapter_x);

        try {

            view = this.findViewById(R.id.webView1);
            view.loadUrl(url_adapter_x);
            Log.e("get_url", "get_url: " + view);
        } catch (LinkageError e) {
            Log.e("LinkageError", "get_url: " + e);
        } catch (Exception e) {
            Log.e("Exception", "get_url: " + e);
        }
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            try {
                view.loadUrl(url);
                Log.e("MyBrowser", "get_url: " + view);
                Log.e("MyBrowser", "get_url: " + url);
            } catch (Exception e) {
                Log.e("MyBrowser", "get_url_error: " + view);
            }

            return true;
        }
    }
}
