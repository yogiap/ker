package com.kominfo.malang.smartcity.pendidikan;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.kominfo.malang.smartcity.R;

public class PerguruanTinggi extends AppCompatActivity {

    WebView webVPe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.infopendidikan);

        webVPe = (WebView)findViewById(R.id.webVP);
        String url = "https://ncctrial.malangkota.go.id/f_kios_v2/beranda/pendidikan/53f85325db3e4f62a90336bf07ff30a26db554224d126e93f3e74990eec67810?menu=c310647df0cb78bb353c5ddfbe55cfbc1e3ab45e5e3491bdd4153625396c3357878b9a9f2b2fbfc4625ed29ab4ed5466f6327dbf740503f85c5906918783c057";
        webVPe.getSettings().setJavaScriptEnabled(true);
        webVPe.setFocusable(true);
        webVPe.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webVPe.getSettings().setDomStorageEnabled(true);
        webVPe.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webVPe.getSettings().setDatabaseEnabled(true);
        webVPe.getSettings().setAppCacheEnabled(true);
        webVPe.loadUrl(url);
        webVPe.setWebViewClient(new WebViewClient());
    }

    @Override
    public void onBackPressed() {
        if (webVPe.canGoBack()) {
            webVPe.goBack();
        }else {
            super.onBackPressed();
        }
    }
}

