package com.kominfo.malang.smartcity.register_new.model_register;

import com.google.gson.annotations.SerializedName;

/**
 * Created by USER on 22/03/2018.
 */

public class List_Response {
    @SerializedName("response")
    String response;
    @SerializedName("message")
    String message;
    @SerializedName("id_user")
    String is_user;

    public List_Response(String response, String message, String is_user) {
        this.response = response;
        this.message = message;
        this.is_user = is_user;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIs_user() {
        return is_user;
    }

    public void setIs_user(String is_user) {
        this.is_user = is_user;
    }
}
