package com.kominfo.malang.smartcity.internalLib;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Created by USER on 16/03/2018.
 */

public class CheckConn {
    //    Context context;
    String TAG = "CheckConn";

    public boolean isConnected(Context context) {


        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        Log.e(TAG, "isConnected: " + isConnected);
        return isConnected;
    }
}
