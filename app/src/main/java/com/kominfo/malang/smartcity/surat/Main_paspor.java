package com.kominfo.malang.smartcity.surat;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import com.kominfo.malang.smartcity.R;

public class Main_paspor extends Activity {

    TextView textView;
    Typeface customFont;
    CardView cv_pasporonline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paspor);

        customFont = Typeface.createFromAsset(getAssets(), "font/MontserratBold.ttf");
        textView = findViewById(R.id.textViewpaspor);
        textView.setTypeface(customFont);

        cv_pasporonline = (CardView) findViewById(R.id.cv_pasporonline);
        cv_pasporonline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iRegister = new Intent(Main_paspor.this, PasporOnline.class);
                startActivity(iRegister);
            }
        });
    }
}
