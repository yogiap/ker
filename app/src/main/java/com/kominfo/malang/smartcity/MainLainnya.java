package com.kominfo.malang.smartcity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;


public class MainLainnya extends Activity {

    CardView cv_kejaksaan, cv_urbn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lain);

        cv_kejaksaan = findViewById(R.id.cv_kejaksaan);

        cv_kejaksaan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainLainnya.this, MainKejaksaan.class);
                startActivity(i);
            }
        });
    }
}
