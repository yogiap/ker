package com.kominfo.malang.smartcity.pasar;

/**
 * Created by arimahardika on 23/02/2018.
 */

public class Model_Pasar {
    private String nama, jenis, satuan, harga_kemarin, tanggal;

    public Model_Pasar(String nama, String jenis, String satuan, String harga_kemarin, String tanggal) {
        this.nama = nama;
        this.jenis = jenis;
        this.satuan = satuan;
        this.harga_kemarin = harga_kemarin;
        this.tanggal = tanggal;
    }

    public String getNama() {
        return nama;
    }

    public String getJenis() {
        return jenis;
    }

    public String getSatuan() {
        return satuan;
    }

    public String getHarga_kemarin() {
        return harga_kemarin;
    }

    public String getTanggal() {
        return tanggal;
    }
}
