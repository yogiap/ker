package com.kominfo.malang.smartcity.pendidikan;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.kominfo.malang.smartcity.R;

public class Sd extends AppCompatActivity {

    WebView webVPe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.infopendidikan);

        webVPe = (WebView)findViewById(R.id.webVP);
        String url = "https://ncctrial.malangkota.go.id/f_kios_v2/beranda/pendidikan/7fd7db09359f9723b6ba6e98be7a685ceae45115d77f5d7026c167a3f6e557b9?menu=f230a450c385ce1f68a31e40fdf43632cbb7bd90997d42f8107573cb6ce4b7bf9c620794ef174c6ca004cdcc569190f5119dc3869911fb8aef49c57a0abc462d";
        webVPe.getSettings().setJavaScriptEnabled(true);
        webVPe.setFocusable(true);
        webVPe.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webVPe.getSettings().setDomStorageEnabled(true);
        webVPe.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webVPe.getSettings().setDatabaseEnabled(true);
        webVPe.getSettings().setAppCacheEnabled(true);
        webVPe.loadUrl(url);
        webVPe.setWebViewClient(new WebViewClient());
    }

    @Override
    public void onBackPressed() {
        if (webVPe.canGoBack()) {
            webVPe.goBack();
        }else {
            super.onBackPressed();
        }
    }
}

