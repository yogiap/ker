package com.kominfo.malang.smartcity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

import com.kominfo.malang.smartcity.login.MainLogin;

/**
 * Created by arimahardika on 17/04/2018.
 */

public class Profile_Main extends Activity {

    Button goToAcc, logout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_main);

        goToAcc = findViewById(R.id.btn_hamburgerIcon_login);
        goToAcc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Profile_Main.this, MainLogin.class);
                startActivity(i);
            }
        });
    }
}
