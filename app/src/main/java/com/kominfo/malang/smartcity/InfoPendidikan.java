package com.kominfo.malang.smartcity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class InfoPendidikan extends AppCompatActivity {

    WebView webVP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.infopendidikan);

        webVP = (WebView)findViewById(R.id.webVP);
        String url = "https://ncctrial.malangkota.go.id/f_kios_v2/beranda/layanan/";
        webVP.getSettings().setJavaScriptEnabled(true);
        webVP.setFocusable(true);
        webVP.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webVP.getSettings().setDomStorageEnabled(true);
        webVP.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webVP.getSettings().setDatabaseEnabled(true);
        webVP.getSettings().setAppCacheEnabled(true);
        webVP.loadUrl(url);
        webVP.setWebViewClient(new WebViewClient());
    }

    @Override
    public void onBackPressed() {
        if (webVP.canGoBack()) {
            webVP.goBack();
        }else {
            super.onBackPressed();
        }
    }
}