package com.kominfo.malang.smartcity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

import com.kominfo.malang.smartcity.transportasi.Angkot;
import com.kominfo.malang.smartcity.transportasi.Stasiun;
import com.kominfo.malang.smartcity.transportasi.Terminal;

public class MainTransportasi extends Activity {

    CardView t_angkot, t_stasiun, t_terminal;
    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_transportasi);

        t_angkot = findViewById(R.id.t_angkot);
        t_stasiun = findViewById(R.id.t_stasiun);
        t_terminal = findViewById(R.id.t_terminal);

        t_angkot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainTransportasi.this, Angkot.class);
                startActivity(i);
            }
        });

        t_stasiun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainTransportasi.this, Stasiun.class);
                startActivity(i);
            }
        });

        t_terminal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainTransportasi.this, Terminal.class);
                startActivity(i);
            }
        });
    }
}
