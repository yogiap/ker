package com.kominfo.malang.smartcity.surat;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import com.kominfo.malang.smartcity.R;

public class Main_surat extends Activity {

    CardView cv_ktp, cv_sim, cv_bpkb, cv_kk, cv_paspor, cv_npwp, cv_stnk;
    TextView textView;
    Typeface customFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_surat);

        customFont = Typeface.createFromAsset(getAssets(), "font/MontserratBold.ttf");
        textView = findViewById(R.id.textViewSurat);
        textView.setTypeface(customFont);

        cv_ktp = findViewById(R.id.cv_ktp);
        cv_sim = findViewById(R.id.cv_sim);
        cv_bpkb = findViewById(R.id.cv_bpkb);
        cv_kk = findViewById(R.id.cv_kk);
        cv_paspor = findViewById(R.id.cv_paspor);
        cv_npwp = findViewById(R.id.cv_npwp);
        cv_stnk = findViewById(R.id.cv_stnk);

        cv_ktp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_surat.this, Main_ktp.class);
                startActivity(i);
            }
        });

        cv_sim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_surat.this, Main_Sim.class);
                startActivity(i);
            }
        });

        cv_bpkb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_surat.this, Main_bpkb.class);
                startActivity(i);
            }
        });

        cv_kk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_surat.this, Main_kk.class);
                startActivity(i);
            }
        });

        cv_paspor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_surat.this, Main_paspor.class);
                startActivity(i);
            }
        });

        cv_npwp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_surat.this, Main_npwp.class);
                startActivity(i);
            }
        });

        cv_stnk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_surat.this, Main_Stnk.class);
                startActivity(i);
            }
        });
    }
}
