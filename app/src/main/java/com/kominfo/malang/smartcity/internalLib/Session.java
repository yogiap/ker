package com.kominfo.malang.smartcity.internalLib;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

//import static android.content.Context.MODE_PRIVATE;

/**
 * Created by USER on 23/03/2018.
 */

public class Session {

    String TAG = "Session";

    public void loginSession(Context context, String id_user, String username, String url_img,
                             String nik, String email, String tlp, String status, String type_penduduk,
                             String nama, String alamat, String wn, String tmp_lhr, String tgl_lhr,
                             String jk) {

        Log.e(TAG, "loginSession: run");
        try {
            Log.e(TAG, "loginSession: run");
            SharedPreferences sharedPref = context.getSharedPreferences("Login", Context.MODE_PRIVATE);
            Log.e(TAG, "loginSession: sharedPref: " + sharedPref);
            SharedPreferences.Editor editor = sharedPref.edit();
            Log.e(TAG, "loginSession: editor: " + editor);

//        editor.putString("name", "Elen);
            //main data
            editor.putString("id_user", id_user);
            editor.putString("username", username);
            editor.putString("url_img", url_img);
            editor.putString("nik", nik);
            editor.putString("email", email);
            editor.putString("tlp", tlp);
            editor.putString("status", status);
            editor.putString("type_penduduk", type_penduduk);

            //second data
            editor.putString("nama", nama);
            editor.putString("alamat", alamat);
            editor.putString("kewarganegaraan", wn);
            editor.putString("tmp_lhr", tmp_lhr);
            editor.putString("tgl_lhr", tgl_lhr);
            editor.putString("jk", jk);

//            editor.putString("pass", pass);

            editor.putBoolean("status_log", true);
            editor.apply();
        } catch (Exception e) {
            Log.e(TAG, "loginSession: " + e.getMessage());
        }
    }

    public void sessionDestroy(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences("Login", Context.MODE_PRIVATE).edit();
        editor.clear();
        editor.commit();
        Log.e(TAG, "sessionDestroy: session destroy");
    }
}
