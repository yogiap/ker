package com.kominfo.malang.smartcity.main_feature;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import com.kominfo.malang.smartcity.R;
import com.kominfo.malang.smartcity._globalVariable.StaticVariable;
import com.kominfo.malang.smartcity.pariwisata.InfoHotel;
import com.kominfo.malang.smartcity.pariwisata.InfoWisata;
import com.kominfo.malang.smartcity.pariwisata.InfoWisataSubMenu;

/**
 * Created by lenovo on 2/5/2018.
 */

public class Pariwisata extends Activity implements View.OnClickListener {

    CardView tempat_wisata, hotel, kuliner, oleh_oleh;
    Intent i;
    Typeface customFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_pariwisata);


        customFont = Typeface.createFromAsset(getAssets(), "font/myriad_pro_regular.otf");
        TextView textView = findViewById(R.id.tv_pariwisata_list);
        textView.setTypeface(customFont);

        tempat_wisata = findViewById(R.id.info_tempat_wisata);
        hotel = findViewById(R.id.info_hotel);
        kuliner = findViewById(R.id.info_kuliner);
        oleh_oleh = findViewById(R.id.info_oleholeh);

        tempat_wisata.setOnClickListener(this);
        hotel.setOnClickListener(this);
        kuliner.setOnClickListener(this);
        oleh_oleh.setOnClickListener(this);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        Intent i;

        switch (v.getId()) {
            case R.id.info_tempat_wisata:
                i = new Intent(this, InfoWisata.class);
                startActivity(i);
                break;
            case R.id.info_hotel:
                i = new Intent(this, InfoHotel.class);
                i.putExtra(StaticVariable.NAMA_LAYANAN, StaticVariable.INFO_HOTEL);
                this.startActivity(i);
                break;
            case R.id.info_kuliner:
                i = new Intent(this, InfoWisataSubMenu.class);
                i.putExtra(StaticVariable.NAMA_LAYANAN, StaticVariable.INFO_KULINER);
                this.startActivity(i);
                break;
            case R.id.info_oleholeh:
                i = new Intent(this, InfoWisataSubMenu.class);
                i.putExtra(StaticVariable.NAMA_LAYANAN, StaticVariable.INFO_OLEH_OLEH);
                this.startActivity(i);
                break;

        }
    }
}