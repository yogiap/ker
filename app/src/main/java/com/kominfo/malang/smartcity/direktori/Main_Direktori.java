package com.kominfo.malang.smartcity.direktori;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

import com.kominfo.malang.smartcity.R;
import com.kominfo.malang.smartcity._globalVariable.StaticVariable;
import com.kominfo.malang.smartcity.event.Event_New_Layout;
import com.kominfo.malang.smartcity.kampus.Main_Kampus;
import com.kominfo.malang.smartcity.main_feature.Pariwisata;
import com.kominfo.malang.smartcity.pariwisata.InfoHotelSubMenu;
import com.kominfo.malang.smartcity.pariwisata.InfoWisataSubMenu;
import com.kominfo.malang.smartcity.sekolah.SekolahSd;
import com.kominfo.malang.smartcity.sekolah.SekolahSma;
import com.kominfo.malang.smartcity.sekolah.SekolahSmp;
import com.kominfo.malang.smartcity.sekolah.SekolahTk;
import com.kominfo.malang.smartcity.transportasi.Stasiun;
import com.kominfo.malang.smartcity.transportasi.Terminal;

public class Main_Direktori extends Activity {

    CardView cv_pariwisata, cv_kuliner, cv_kampus, cv_terminal, cv_stasiun, cv_mall,
    cv_pasar, cv_oleh, cv_heritage, cv_komunitas, cv_hotel, cv_event,cv_tk,cv_sd,cv_smp,cv_sma;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_direktori);

        cv_pariwisata = findViewById(R.id.cv_pariwisata);
        cv_kuliner = findViewById(R.id.cv_kuliner);
        cv_kampus = findViewById(R.id.cv_kampus);
        cv_terminal = findViewById(R.id.cv_terminal);
        cv_stasiun = findViewById(R.id.cv_stasiun);
        cv_mall = findViewById(R.id.cv_mall);
        cv_pasar = findViewById(R.id.cv_pasar);
        cv_oleh = findViewById(R.id.cv_oleh);
        cv_heritage = findViewById(R.id.cv_heritage);
        cv_hotel = findViewById(R.id.cv_hotel);
        cv_event = findViewById(R.id.cv_event);
        cv_tk = findViewById(R.id.cv_tk);
        cv_sd = findViewById(R.id.cv_sd);
        cv_smp = findViewById(R.id.cv_smp);
        cv_sma = findViewById(R.id.cv_sma);

        cv_pariwisata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Direktori.this, Pariwisata.class);
                startActivity(i);
            }
        });

        cv_kuliner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Direktori.this, InfoWisataSubMenu.class);
                i.putExtra(StaticVariable.NAMA_LAYANAN, StaticVariable.INFO_KULINER);
                startActivity(i);
            }
        });

        cv_kampus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Direktori.this, Main_Kampus.class);
                startActivity(i);
            }
        });

        cv_terminal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Direktori.this, Terminal.class);
                startActivity(i);
            }
        });

        cv_stasiun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Direktori.this, Stasiun.class);
                startActivity(i);
            }
        });

        cv_mall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Direktori.this, InfoWisataSubMenu.class);
                i.putExtra(StaticVariable.NAMA_LAYANAN, StaticVariable.INFO_BELANJA);
                startActivity(i);
            }
        });

        cv_pasar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Direktori.this, Main_pasart.class);
                startActivity(i);
            }
        });

        cv_oleh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Direktori.this, InfoWisataSubMenu.class);
                i.putExtra(StaticVariable.NAMA_LAYANAN, StaticVariable.INFO_OLEH_OLEH);
                startActivity(i);
            }
        });

        cv_heritage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Direktori.this, Main_Heritage.class);
                startActivity(i);
            }
        });

        cv_hotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Direktori.this, InfoHotelSubMenu.class);
                i.putExtra(StaticVariable.NAMA_LAYANAN, StaticVariable.INFO_HOTEL);
                startActivity(i);
            }
        });

        cv_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Direktori.this, Event_New_Layout.class);
                startActivity(i);
            }
        });

        cv_tk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Direktori.this, SekolahTk.class);
                startActivity(i);
            }
        });
        cv_sd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Direktori.this, SekolahSd.class);
                startActivity(i);
            }
        });
        cv_smp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Direktori.this, SekolahSmp.class);
                startActivity(i);
            }
        });
        cv_sma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Direktori.this, SekolahSma.class);
                startActivity(i);
            }
        });

    }
}
