package com.kominfo.malang.smartcity.lowongan_pekerjaan;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.kominfo.malang.smartcity.R;
import com.kominfo.malang.smartcity._globalVariable.MessageVariabe;
import com.kominfo.malang.smartcity._globalVariable.URLCollection;
import com.kominfo.malang.smartcity.lowongan_pekerjaan.model_loker.ListIdrekan;
import com.kominfo.malang.smartcity.lowongan_pekerjaan.model_loker.MainResponse;
import com.kominfo.malang.smartcity.internalLib.RetrofitLib.base_url.Base_url;
import com.kominfo.malang.smartcity.internalLib.RetrofitLib.request_management.SendDataLoker;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by USER on 05/06/2018.
 */

public class MainIdrekan extends AppCompatActivity {

    String DATA_SOURCE_URL = URLCollection.BASE_URL_IDREKAN;
    MessageVariabe messageVariabe = new MessageVariabe();
    String TOKEN_MOBILE = "X00W",
            TAG = "MainIdrekan";

    Call<MainResponse> getdata;
    SendDataLoker base_url_management;
    ProgressDialog progressDialog;

    LinearLayout ll_sumber_loker;
    RecyclerView recyclerView;
    List<ListIdrekan> list_loker = new ArrayList<>();
    private RecyclerView.Adapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_loker);

        ll_sumber_loker = findViewById(R.id.ll_sumber_loker);
        recyclerView = findViewById(R.id.loker_recyclerView);
        recyclerView.hasFixedSize();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e(TAG, "onStart: start");
        getDataLoker();
    }

    @Override
    protected void onDestroy() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        super.onDestroy();
    }

    private void getDataLoker() {
        progressDialog = new ProgressDialog(this) {
            @Override
            public void onBackPressed() {
                finish();
            }
        };

        progressDialog.setMessage(MessageVariabe.MESSAGE_LOADING);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();

        base_url_management = Base_url.getClient(DATA_SOURCE_URL).create(SendDataLoker.class);

        getdata = base_url_management.getIdrekanDatax(TOKEN_MOBILE);

        adapterRequest(getdata);

    }

    public void adapterRequest_aw(Call<ResponseBody> getdata) {
        Log.e(TAG, "adapterRequest: run");
        getdata.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                progressDialog.dismiss();
                try {
//                    list_loker = response.body().getBody();
                    //home = 0; dashbrd = 1; search = 2;
                    JSONObject dataJson = new JSONObject(response.body().string());

                    Log.e(TAG, "onResponse: " + dataJson.toString());
                    Log.e(TAG, "onResponse: body: " + dataJson.getJSONArray("body").get(0));

                    JSONObject bodyJson = new JSONObject(dataJson.getJSONArray("body").get(0).toString());
                    Log.e(TAG, "onResponse: " + bodyJson.getJSONArray("syarat_umum").get(0));

                    //JSONArray data_syarat_umum = new JSONArray(dataJson.getJSONObject("syarat_umum"));
                    //Log.e(TAG, "onResponse: syarat_umum"+ data_syarat_umum.get(0));
//                    mAdapter = new Adapter_Loker(list_loker, Main_Loker.this);
//                    recyclerView.setAdapter(mAdapter);
//                    mAdapter.notifyDataSetChanged();

                } catch (Exception e) {
                    Log.e(TAG, "onResponse: " + e);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(MainIdrekan.this, "Request Failure Please Refresh your Apps :)", Toast.LENGTH_LONG).show();
                Log.e(TAG, "onFailure: " + t);
            }
        });
    }

    public void adapterRequest(Call<MainResponse> getdata) {
        Log.e(TAG, "adapterRequest: run");
        getdata.enqueue(new Callback<MainResponse>() {
            @Override
            public void onResponse(Call<MainResponse> call, retrofit2.Response<MainResponse> response) {
                progressDialog.dismiss();
                try {
                    list_loker = response.body().getBody();
                    //home = 0; dashbrd = 1; search = 2;
                    Log.e(TAG, "onResponse: " + response.body().getBody().get(0).getSyarat_umum());
//                    List<String> strings = response.body().getBody().get(0).getSyarat_umum();
//                    Log.e(TAG, "onResponse: "+strings.get(0));

                    mAdapter = new AdapterIdrekan(list_loker, MainIdrekan.this);
                    recyclerView.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();

                } catch (Exception e) {
                    Log.e(TAG, "onResponse: " + e);
                }
            }

            @Override
            public void onFailure(Call<MainResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(MainIdrekan.this, "Request Failure Please Refresh your Apps :)", Toast.LENGTH_LONG).show();
                Log.e(TAG, "onFailure: " + t);
            }
        });
    }
}
