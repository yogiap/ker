package com.kominfo.malang.smartcity.register_new;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.CalendarContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.kominfo.malang.smartcity.R;
import com.kominfo.malang.smartcity._globalVariable.URLCollection;
import com.kominfo.malang.smartcity.internalLib.CheckConn;
import com.kominfo.malang.smartcity.internalLib.RetrofitLib.base_url.Base_url;
import com.kominfo.malang.smartcity.internalLib.RetrofitLib.request_management.SendDataLogin;
import com.kominfo.malang.smartcity.internalLib.refresh_activity.RefreshDataUser;
import com.kominfo.malang.smartcity.internalLib.register.MainRegisterData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by USER on 27/03/2018.
 **/

public class TakePhoto_new extends FragmentActivity {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_IMAGE_GALLERY = 2;
    String BASE_URL = URLCollection.DATA_SOURCE_LOKER;
    String TOKEN_MOBILE = "X00W";
    String nik = "",
            action = "";
    ProgressDialog progressDialog;
    Call<ResponseBody> getdata;
    SendDataLogin base_url_management;
    String message_galat = "inputkan data dengan benar",
            response_galat = "failed input data, coba lagi nanti",
            loadingMessage = "mohon tunggu proses sedang berjalan...";
    CheckConn checkConn = new CheckConn();
    Intent intent;
    String TAG = "TakePhoto_new";
    ImageButton foto;
    Button btn_save, btn_cancle;
    ImageView btnGallery, btnPhoto;
    File mFileUri;

    MainRegisterData tempMainRegData = new MainRegisterData();

    String status_warga = null;

    int ty = 0;
    Bitmap originalImage;
    int width;
    int height;
    int newWidth = 850;
    int newHeight = 700;
    Matrix matrix;
    Bitmap resizedBitmap;
    float scaleWidth;
    float scaleHeight;
    String imagePath = "";

    private static File getMediaFileName() {
        // Lokasi External sdcard

        File mediaStorageDir = new
                File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "Tanggap");
        // Buat directori tidak direktori tidak eksis
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.e("CameraDemo", "Gagal membuat directory" + "CameraDemo");
                return null;
            }
        }
        File mediaFile = null;
        // Membuat nama file
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + "TGP_IMG_" + timeStamp
                + ".jpg");
        Log.e("CameraDemo", "getMediaFileName: " + mediaFile);
        return mediaFile;
    }

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_take_photo);

        foto = (ImageButton) findViewById(R.id.foto);
        btn_save = (Button) findViewById(R.id.btnNext);
        btn_cancle = (Button) findViewById(R.id.btnPrev);

        btnGallery = (ImageView) findViewById(R.id.btnGalery);
        btnPhoto = (ImageView) findViewById(R.id.btnPhoto);

        try {
            intent = getIntent();
            nik = intent.getStringExtra("nik");
            ty = Integer.parseInt(intent.getStringExtra("ty_take_img"));
            Log.e(TAG, "onCreate: " + nik);
            Log.e(TAG, "onCreate: " + ty);
        } catch (Exception e) {
            Log.e(TAG, "onCreate: " + e);
        }
        //nik = "0";
        String[] dataSession = tempMainRegData.MainRegisterGet(this);
        if (dataSession != null) {
            status_warga = dataSession[4];
        }

        foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                imageSelect(TakePhoto_new.this);
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e(TAG, "onClick: run save");
                if (checkConn.isConnected(TakePhoto_new.this)) {
                    Log.e(TAG, "onClick: isConnected: run");
                    sendDataPhoto();
                }
            }
        });

        btn_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, REQUEST_IMAGE_GALLERY);
            }
        });

        btnPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e(TAG, "onClick: 0");
                dispatchTakePictureIntent();
            }
        });
    }

    public void imageSelect(Context context) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Pilih Foto Anda");
        builder.setItems(new CharSequence[]{"Camera", "Gallery", "Cancle"},
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                Log.e(TAG, "onClick: 0");
                                dispatchTakePictureIntent();
                                break;
                            case 1:
                                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                                photoPickerIntent.setType("image/*");
                                startActivityForResult(photoPickerIntent, REQUEST_IMAGE_GALLERY);
                                break;
                            case 2:
                                finish();
                                break;
                        }
                    }
                });
        builder.create().show();
    }

    private void dispatchTakePictureIntent() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

            if (ActivityCompat.checkSelfPermission(TakePhoto_new.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(TakePhoto_new.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(TakePhoto_new.this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 10);
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 11);
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CALENDAR}, 13);

            }
            mFileUri = getMediaFileName();
            //error nuget
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, CalendarContract.CalendarCache.URI.fromFile(mFileUri));
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String filePath = "";
        Log.e(TAG, "onActivityResult: mFileUri:" + requestCode);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                filePath = mFileUri.getPath();
                Log.e(TAG, "onActivityResult: mFileUri:" + filePath);
            } else if (requestCode == 2) {
                if (data == null) {
                    Toast.makeText(getApplicationContext(), "Gambar Gagal Di load",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                Uri selectedImage = data.getData();
                Log.e(TAG, "onActivityResult: " + selectedImage);
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);

                if (cursor != null) {
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    filePath = cursor.getString(columnIndex);
                    mFileUri = getMediaFileName();
                    cursor.close();
                } else {
                    Toast.makeText(getApplicationContext(), "Gambar Gagal Di load",
                            Toast.LENGTH_LONG).show();
                }
            }

            try {
                Bitmap bitmapLoaded = BitmapFactory.decodeFile(filePath);
                Log.e(TAG, "onActivityResult: mFileUri: " + mFileUri.getPath());
                int width = bitmapLoaded.getWidth();
                int height = bitmapLoaded.getHeight();

                float scaleWidth = ((float) newWidth) / width;
                float scaleHeight = ((float) newHeight) / height;

                Matrix bitmapMatrix = new Matrix();
                bitmapMatrix.postScale(scaleWidth, scaleHeight);

                if (Build.VERSION.SDK_INT < 21) {
                    bitmapMatrix.postRotate(90);
                }

                Bitmap resizedBitmap = Bitmap.createBitmap(bitmapLoaded, 0, 0, width, height, bitmapMatrix, true);

                FileOutputStream out = null;
                String filename = mFileUri.getPath();

                Log.e(TAG, "onActivityResult: filename: " + filename);

                try {

                    out = new FileOutputStream(filename);
                    Log.e(TAG, "onActivityResult: FileOutputStream: " + out);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
                foto.setImageBitmap(resizedBitmap);
                imagePath = mFileUri.getPath();

            } catch (Exception e) {
                Log.e("PHOTOPATH", "LOAD IMAGE FAILED - " + e.toString());
                Toast.makeText(this, "LOAD IMAGE FAILED", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void sendDataPhoto() {
        progressDialog = new ProgressDialog(this) {
            @Override
            public void onBackPressed() {
                finish();
            }
        };

        Log.e(TAG, "sendDataPhoto: run");
        progressDialog.setMessage(loadingMessage);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();

        MultipartBody.Part body = null;
        if (!imagePath.isEmpty()) {
            File file = new File(imagePath);

            Log.e(TAG, "sendDataPhoto: " + file);
            RequestBody requestFile_ = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            body = MultipartBody.Part.createFormData("gambar", file.getName(), requestFile_);

            RequestBody token_ = MultipartBody.create(MediaType.parse("multipart/form-data"), TOKEN_MOBILE);
            RequestBody id_user_ = MultipartBody.create(MediaType.parse("multipart/form-data"), nik);
            RequestBody action_ = MultipartBody.create(MediaType.parse("multipart/form-data"), String.valueOf(ty));

            base_url_management = Base_url.getClient(BASE_URL)
                    .create(SendDataLogin.class);

            getdata = base_url_management.RegisterUpdateImg(id_user_, token_, action_, body);

            adapterRequest(getdata);
        } else {
            progressDialog.dismiss();
        }

    }

    public void adapterRequest(Call<ResponseBody> getdata) {
        Log.e(TAG, "adapterRequest: run");
        getdata.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                progressDialog.dismiss();
                JSONObject dataJson = null;
                String message_out = null;
                try {
                    dataJson = new JSONObject(response.body().string());
                    String status = dataJson.getJSONObject("body_message").getString("status");
                    message_out = dataJson.getJSONObject("body_message").getString("message");
                    Log.e(TAG, "onResponse: " + status);
                    if (Boolean.valueOf(status)) {
                        Log.e(TAG, "onResponse: " + dataJson);

                        if (ty != 0) {
                            new RefreshDataUser().create_session(dataJson.getJSONObject("body"), TakePhoto_new.this);

                        }
                        finish();
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "onResponse: " + e.getMessage());
                } catch (IOException e) {
                    Log.e(TAG, "onResponse: " + e.getMessage());
                }
                Toast.makeText(TakePhoto_new.this, message_out, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Log.e(TAG, "onFailure: " + t.getMessage());
                progressDialog.dismiss();

            }
        });

    }
}
