package com.kominfo.malang.smartcity.surat;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import com.kominfo.malang.smartcity.R;

public class Main_Sim extends Activity {

    TextView textView;
    Typeface customFont;
    CardView cv_simonline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sim);

        customFont = Typeface.createFromAsset(getAssets(), "font/MontserratBold.ttf");
        textView = findViewById(R.id.textViewsim);
        textView.setTypeface(customFont);

        cv_simonline = (CardView) findViewById(R.id.cv_simonline);
        cv_simonline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iRegister = new Intent(Main_Sim.this, SimOnline.class);
                startActivity(iRegister);
            }
        });
    }
}
