package com.kominfo.malang.smartcity._globalVariable;

/**
 * Created by USER on 24/04/2018.
 */

public class MessageVariabe {

    public final static String MESSAGE_ERROR_RESPONSE_FAIL_ANDROID = "Mohon Maaf ..terjadi kesalahan saat pengiriman komunikasi data, " +
            "pastikan jaringan anda aktif saat menjalankan aplikasi ini..";
    public final static String MESSAGE_ERROR_RESPONSE_FAIL_SERVER = "Mohon Maaf ..terjadi kesalahan pada server saat komunikasi data, " +
            "pastikan jaringan anda aktif saat menjalankan aplikasi ini..";
    public final static String MESSAGE_ERROR_INPUT = "Input anda tidak sesuai. mohon untuk memeriksa inputan anda sekali lagi";
    public final static String MESSAGE_ERROR_INPUT_PASSWORD = "Input password dan Ulangi password harus sama mohon periksa kembali";
    public final static String MESSAGE_ERROR_DISCONNECTED = "Untuk mengakses aplikasi ini membutukan koneksi jaringan. Mohon aktifkan jaringan anda";
    public final static String MESSAGE_ERROR_GPS = "Aplikasi ini membutuhkan akses terhadap GPS anda. Mohon Aktifkan GPS anda";
    public final static String MESSAGE_ERROR_LOGIN = "Login gagal, Silahkan periksa username dan password anda";

    public final static String MESSAGE_SUCCESS_INPUT = "Input data sukses";
    public final static String MESSAGE_SUCCESS_LOGIN = "Login berhasil. SELAMAT DATANG DI APLIAKSI SIDIA";
    public final static String MESSAGE_SUCCESS_REGISTER = "Proses registrasi anda telah berhasil. Silahkan lakukan verifikasi email dan KTP";
    public final static String MESSAGE_SUCCESS_REGISTER_KTP = "Proses verifikasi KTP anda akan di proses tunggu beberapa saat untuk persetujuan dari admin";

    public final static String MESSAGE_LOADING = "Proses sedang berjalan.. Mohon tunggu beberapa saat..";
    public final static String MESSAGE_EMAIL_CHECK = "Perubahan telah di terima.. " +
            "Untuk mengaktifkan perubahan silahkan periksa email baru saudara..";

    public final static String MESSAGE_ERROR_ACT_TANGGAP = "Mohon aktivasi user anda terlebih dahulu, silahkan periksa email sudara";

}
