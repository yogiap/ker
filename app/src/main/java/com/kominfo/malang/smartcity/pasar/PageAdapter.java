package com.kominfo.malang.smartcity.pasar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.kominfo.malang.smartcity.pasar.fragment.pasar_fragment_bahan_bangunan;
import com.kominfo.malang.smartcity.pasar.fragment.pasar_fragment_panen;
import com.kominfo.malang.smartcity.pasar.fragment.pasar_fragment_sembako;

/**
 * Created by arimahardika on 11/04/2018.
 */

public class PageAdapter extends FragmentPagerAdapter {

    private int numberOfTabs;

    public PageAdapter(FragmentManager fm, int numberOfTabs) {
        super(fm);
        this.numberOfTabs = numberOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new pasar_fragment_sembako();
            case 1:
                return new pasar_fragment_panen();
            case 2:
                return new pasar_fragment_bahan_bangunan();
        }
        return null;
    }

    @Override
    public int getCount() {
        return numberOfTabs;
    }
}
