package com.kominfo.malang.smartcity.surat;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.kominfo.malang.smartcity.R;

public class PasporOnline extends AppCompatActivity {

    WebView webV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview);

        webV = (WebView) findViewById(R.id.webView1);
        String url = "https://antrian.imigrasi.go.id/";
        webV.getSettings().setJavaScriptEnabled(true);
        webV.setFocusable(true);
        webV.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webV.getSettings().setDomStorageEnabled(true);
        webV.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webV.getSettings().setDatabaseEnabled(true);
        webV.getSettings().setAppCacheEnabled(true);
        webV.loadUrl(url);
        webV.setWebViewClient(new WebViewClient());
    }

    @Override
    public void onBackPressed() {
        if (webV.canGoBack()) {
            webV.goBack();
        } else {
            super.onBackPressed();
        }
    }
}

