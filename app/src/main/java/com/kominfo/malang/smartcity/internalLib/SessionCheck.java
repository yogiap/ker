package com.kominfo.malang.smartcity.internalLib;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by USER on 23/03/2018.
 */

public class SessionCheck {
    String TAG = "SessionCheck";

    public String[] seesionLoginChecker(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("Login", Context.MODE_PRIVATE);
        Log.e(TAG, "seesionLoginChecker: " + prefs);
        if (prefs != null) {
            Boolean status = prefs.getBoolean("status_log", false);
            if (status != null && status != false) {
                String id_user = prefs.getString("id_user", null);//"No name defined" is the default value.
                String username = prefs.getString("username", null); //0 is the default value.
                String url_img = prefs.getString("url_img", null); //0 is the default value.
                String nik = prefs.getString("nik", null);//"No name defined" is the default value.
                String email = prefs.getString("email", null); //0 is the default value.
                String tlp = prefs.getString("tlp", null);
                String status_pro = prefs.getString("status", null);//"No name defined" is the default value.
                String type_penduduk = prefs.getString("type_penduduk", null); //0 is the default value.


                String nama = prefs.getString("nama", null);
                String alamat = prefs.getString("alamat", null);//"No name defined" is the default value.
                String wn = prefs.getString("kewarganegaraan", null); //0 is the default value.
                String tmp_lhr = prefs.getString("tmp_lhr", null);
                String tgl_lhr = prefs.getString("tgl_lhr", null);//"No name defined" is the default value.
                String jk = prefs.getString("jk", null); //0 is the default value.

//                String pass = prefs.getString("pass", null);

                Log.e(TAG, "seesionLoginChecker: " + id_user);
                String[] session_val = {String.valueOf(status),
                        id_user, username, url_img, nik, email, tlp, status_pro, type_penduduk,
                        nama, alamat, wn, tmp_lhr, tgl_lhr, jk};
                return session_val;
            }
        }
        return null;
    }


}
