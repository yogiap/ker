package com.kominfo.malang.smartcity.event;

import com.google.gson.annotations.SerializedName;

/**
 * Created by USER on 30/05/2018.
 */

public class Result {
    @SerializedName("result")
    public String result;

    public String getResult() {
        return this.result;
    }

    public void setResult(String result) {
        this.result = result;
    }

}
