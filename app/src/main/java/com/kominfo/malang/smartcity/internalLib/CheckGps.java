package com.kominfo.malang.smartcity.internalLib;

import android.content.Context;
import android.location.LocationManager;
import android.util.Log;

/**
 * Created by USER on 16/03/2018.
 */

public class CheckGps {
    LocationManager locationManager;
    boolean status = false;
    Context context;

    String TAG = "CheckGps";

    public boolean check_gps(Context context) {
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            Log.e(TAG, "check_gps: " + gps_enabled);
        } catch (Exception ex) {
            Log.e(TAG, "check_gps: " + ex);
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            Log.e(TAG, "check_gps: " + network_enabled);
        } catch (Exception ex) {
            Log.e(TAG, "check_gps: " + ex);
        }

        if (gps_enabled == true || network_enabled == true) {
            return true;
        }

        return false;
    }

}
