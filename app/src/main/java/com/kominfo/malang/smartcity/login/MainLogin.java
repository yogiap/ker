package com.kominfo.malang.smartcity.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.kominfo.malang.smartcity.MainActivity;
import com.kominfo.malang.smartcity.R;
import com.kominfo.malang.smartcity._globalVariable.MessageVariabe;
import com.kominfo.malang.smartcity._globalVariable.URLCollection;
import com.kominfo.malang.smartcity.dashAkun.MainAkun;
import com.kominfo.malang.smartcity.internalLib.CheckConn;
import com.kominfo.malang.smartcity.internalLib.RetrofitLib.base_url.Base_url;
import com.kominfo.malang.smartcity.internalLib.RetrofitLib.request_management.SendDataLogin;
import com.kominfo.malang.smartcity.internalLib.SessionCheck;
import com.kominfo.malang.smartcity.internalLib.TextSafety;
import com.kominfo.malang.smartcity.internalLib.refresh_activity.RefreshDataUser;
import com.kominfo.malang.smartcity.register_new.MainRegister_new;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by USER on 22/03/2018.
 */

public class MainLogin extends FragmentActivity {
    MessageVariabe messageVariabe = new MessageVariabe();
    String BASE_URL = URLCollection.DATA_SOURCE_LOKER;

    String TOKEN_MOBILE = "X00W";
    String TAG = "MainLogin";

    Call<ResponseBody> getdata;
    SendDataLogin base_url_management;

    ProgressDialog progressDialog;

    TextView btnLogin, btnRegister;
    EditText eUsrnm, ePass;
    Intent intent;

    CheckConn checkConn = new CheckConn();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_main);


        eUsrnm = (EditText) findViewById(R.id.et_eUser);
        ePass = (EditText) findViewById(R.id.et_ePass);

        btnLogin = (TextView) findViewById(R.id.btn_login);
        btnRegister = (TextView) findViewById(R.id.btn_register);

        String[] data_session = new SessionCheck().seesionLoginChecker(MainLogin.this);
        Log.e(TAG, "onClick: " + data_session);
        if (data_session != null) {
            Log.e(TAG, "onClick: " + data_session[0]);

            if (Boolean.valueOf(data_session[0])) {
                intent = new Intent(MainLogin.this, MainAkun.class);
                startActivity(intent);
                finish();
            }
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog = new ProgressDialog(MainLogin.this) {
                    @Override
                    public void onBackPressed() {
                        finish();
                    }
                };

                progressDialog.setMessage(messageVariabe.MESSAGE_LOADING);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCancelable(false);
                progressDialog.show();

                boolean checkUserName = new TextSafety().get_valid_char(
                        eUsrnm.getText().toString());
                boolean checkPass = new TextSafety().get_valid_char(
                        ePass.getText().toString());
                if (checkPass == true && checkUserName == true) {
                    if (checkConn.isConnected(MainLogin.this)) {
                        base_url_management = Base_url.getClient_notGson(BASE_URL)
                                .create(SendDataLogin.class);
                        getdata = base_url_management.loginRequest(eUsrnm.getText().toString(),
                                ePass.getText().toString(), TOKEN_MOBILE);

                        setLogin(getdata);
                    } else {
                        Toast.makeText(MainLogin.this, messageVariabe.MESSAGE_ERROR_DISCONNECTED
                                , Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }
                } else {
                    Toast.makeText(MainLogin.this, messageVariabe.MESSAGE_ERROR_LOGIN,
                            Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }

            }
        });


        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(MainLogin.this, MainRegister_new.class);
                startActivity(intent);
                finish();
            }
        });


    }

    public void setLogin(Call<ResponseBody> getdata) {
        Log.e(TAG, "adapterRequest: run");
        getdata.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                progressDialog.dismiss();
                JSONObject dataJson = null;
                try {
                    try {
                        dataJson = new JSONObject(response.body().string());

                        //Log.e(TAG, "onResponse: "+dataJson.toString());
                        String status = null;
                        if (dataJson != null) {

                            JSONObject dataMessage = dataJson.getJSONObject("body_message");
                            String status_message = dataMessage.getString("status");

                            if (status_message != null && Boolean.valueOf(status_message) == true) {
                                JSONObject dataContent = dataJson.getJSONObject("body");
                                //Log.e(TAG, "onResponse: "+dataContent.toString());
                                boolean returnData = new RefreshDataUser().create_session(dataContent, MainLogin.this);
                                Log.e(TAG, "onResponse: " + returnData);
                                if (returnData) {
                                    progressDialog.dismiss();
                                    intent = new Intent(MainLogin.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                    Toast.makeText(MainLogin.this, messageVariabe.MESSAGE_SUCCESS_LOGIN,
                                            Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Log.e(TAG, "onResponse: null");
                                Toast.makeText(MainLogin.this, MessageVariabe.MESSAGE_ERROR_LOGIN,
                                        Toast.LENGTH_LONG).show();
                            }

                            Log.e(TAG, "onResponse: " + dataJson);

                        } else {
                            Toast.makeText(MainLogin.this, MessageVariabe.MESSAGE_ERROR_LOGIN,
                                    Toast.LENGTH_LONG).show();
                            Log.e(TAG, "onResponse: null");
                        }

                    } catch (IOException e) {
                        Toast.makeText(MainLogin.this, MessageVariabe.MESSAGE_ERROR_RESPONSE_FAIL_SERVER,
                                Toast.LENGTH_LONG).show();
                        Log.e(TAG, "onResponse: " + e.getMessage());
                    }

                } catch (JSONException e) {
                    Toast.makeText(MainLogin.this, MessageVariabe.MESSAGE_ERROR_RESPONSE_FAIL_SERVER,
                            Toast.LENGTH_LONG).show();

                    Log.e(TAG, "onResponse: dataJson: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
                Log.e(TAG, "onFailure: " + t);
                Toast.makeText(MainLogin.this, MessageVariabe.MESSAGE_ERROR_RESPONSE_FAIL_ANDROID,
                        Toast.LENGTH_LONG).show();
            }
        });

    }
}
