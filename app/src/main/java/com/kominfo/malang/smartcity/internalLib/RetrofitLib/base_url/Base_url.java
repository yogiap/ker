package com.kominfo.malang.smartcity.internalLib.RetrofitLib.base_url;

import android.util.Log;

import com.kominfo.malang.smartcity._globalVariable.URLCollection;
import com.kominfo.malang.smartcity.internalLib.UnsafeOkHttpClient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by USER on 13/03/2018.
 */

public class Base_url {

//    public static final String BASE_URL = "http://resttanggap.000webhostapp.com/index.php/";
//    public static final String BASE_URL = "http://192.168.86.79:8080/ncc_admin/";

    static String TAG = "Base_url";
    private static Retrofit retrofit = null;

    public static Retrofit getClient(String url) {
        if (retrofit == null) {

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

        }
        return retrofit;
    }

    public static Retrofit getClient_notGson(String baseUrl) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }

        Log.e(TAG, "getClient: " + retrofit);
        return retrofit;

    }

    public static Retrofit getClient_https(String url) {
        if (retrofit == null) {
            OkHttpClient okHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient();

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(url)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

//            retrofit = builder.build();

        }
        return retrofit;
    }

    public static void initializeRetrofit(){
        retrofit = new Retrofit.Builder()
                .baseUrl(URLCollection.BASE_URL_PBB)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
