package com.kominfo.malang.smartcity.internalLib.RetrofitLib.request_management;

/**
 * Created by USER on 12/03/2018.
 */

import com.kominfo.malang.smartcity.tanggap.tanggap_model.ResponseGet;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


public interface SendDataTanggap {
    //insert
    @Multipart
    @POST("tanggap/Apitanggap/insdata")
    Call<ResponseBody> insertData(
            @Part("id_user") RequestBody id_user,
            @Part("id_type") RequestBody id_type,
            @Part("loc") RequestBody loc,
            @Part("desc_ke") RequestBody desc_ke,
            @Part MultipartBody.Part file,
            @Part("token") RequestBody token);

    //get for dashboard
    @Multipart
    @POST("tanggap/Apitanggap/get_data_dash")
    Call<ResponseGet> getItemDash(@Part("id_user") RequestBody id_user,
                                  @Part("token") RequestBody token);

    //get for dashboard
    @Multipart
    @POST("tanggap/Apitanggap/get_data_cate")
    Call<ResponseGet> getItemCa(@Part("id_type") RequestBody id_type,
                                @Part("token") RequestBody token);

    //get for home
    @Multipart
    @POST("tanggap/Apitanggap/get_data_all")
    Call<ResponseGet> getItemAll(@Part("token") RequestBody token);
}
