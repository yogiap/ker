package com.kominfo.malang.smartcity.lowongan_pekerjaan.model_loker;

import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by USER on 05/06/2018.
 */

public class MainResponse {

    @SerializedName("id_resopnse")
    String id_resopnse;

    @SerializedName("body_message")
    JSONObject body_message;

    @SerializedName("body")
    List<ListIdrekan> body;


    public String getId_resopnse() {
        return id_resopnse;
    }

    public void setId_resopnse(String id_resopnse) {
        this.id_resopnse = id_resopnse;
    }

    public JSONObject getBody_message() {
        return body_message;
    }

    public void setBody_message(JSONObject body_message) {
        this.body_message = body_message;
    }

    public List<ListIdrekan> getBody() {
        return body;
    }

    public void setBody(List<ListIdrekan> body) {
        this.body = body;
    }
}
