package com.kominfo.malang.smartcity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import com.kominfo.malang.smartcity._globalVariable.StaticVariable;
import com.kominfo.malang.smartcity.info_layanan.kesehatan.Main_Kesehatan;

public class Main_tahes extends Activity {

    CardView cv_ambulan, cv_dokter, cv_klinik,cv_dokter_gigi;
    TextView textView;
    Typeface customFont;
    String NO_AMBULAN = "119";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tahes);

        customFont = Typeface.createFromAsset(getAssets(), "font/MontserratBold.ttf");
        textView = findViewById(R.id.textViewTahes);
        textView.setTypeface(customFont);

        cv_ambulan = findViewById(R.id.cv_ambulan);
        cv_dokter = findViewById(R.id.cv_dokterumum);
        cv_dokter_gigi = findViewById(R.id.cv_doktergigi);
        cv_klinik = findViewById(R.id.cv_klinik);

        callAction();
    }

    private void callAction() {
        cv_ambulan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent call = new Intent(Intent.ACTION_DIAL);
                call.setData(Uri.parse("tel:" + NO_AMBULAN));
                startActivity(call);
            }
        });

        cv_dokter_gigi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_tahes.this, Main_Kesehatan.class);
                i.putExtra(StaticVariable.NAMA_LAYANAN, StaticVariable.DOKTER_GIGI);
                startActivity(i);
            }
        });
        cv_dokter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_tahes.this, DokterUmum.class);

                startActivity(i);
            }
        });

        cv_klinik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_tahes.this, Main_Kesehatan.class);
                i.putExtra(StaticVariable.NAMA_LAYANAN, StaticVariable.KLINIK);
                startActivity(i);
            }
        });
    }
}
