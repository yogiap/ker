package com.kominfo.malang.smartcity.pajak;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

import com.kominfo.malang.smartcity.R;

public class Pajak_Bphtb extends Activity {

    TextView textView;
    Typeface customFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pajakbphtb);

        customFont = Typeface.createFromAsset(getAssets(), "font/MontserratBold.ttf");
        textView = findViewById(R.id.textViewbphtb);
        textView.setTypeface(customFont);
    }
}
