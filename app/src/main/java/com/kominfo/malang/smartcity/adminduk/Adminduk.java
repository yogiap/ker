package com.kominfo.malang.smartcity.adminduk;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import com.kominfo.malang.smartcity.MainKejaksaan;
import com.kominfo.malang.smartcity.MainLainnya;
import com.kominfo.malang.smartcity.R;
import com.kominfo.malang.smartcity.surat.Main_ktp;
import com.kominfo.malang.smartcity.surat.Main_surat;

public class Adminduk extends Activity {

    CardView cv_lowokwaru, cv_blimbing, cv_klojen, cv_kedungkandang, cv_sukun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adminduk);

        cv_lowokwaru     = findViewById(R.id.cv_lowokwaru);
        cv_blimbing      = findViewById(R.id.cv_blimbing);
        cv_klojen        = findViewById(R.id.cv_klojen);
        cv_kedungkandang = findViewById(R.id.cv_kedungkandang);
        cv_sukun         = findViewById(R.id.cv_sukun);


        cv_lowokwaru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Adminduk.this, PdfLowokwaru.class);
                startActivity(i);
            }
        });
        cv_blimbing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Adminduk.this, PdfBlimbing.class);
                startActivity(i);
            }
        });
        cv_klojen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Adminduk.this, PdfKlojen.class);
                startActivity(i);
            }
        });
        cv_kedungkandang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Adminduk.this, PdfKedungkandang.class);
                startActivity(i);
            }
        });
        cv_sukun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Adminduk.this, PdfSukun.class);
                startActivity(i);
            }
        });
    }
}
