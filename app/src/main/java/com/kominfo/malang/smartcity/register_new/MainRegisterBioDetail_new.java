package com.kominfo.malang.smartcity.register_new;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.kominfo.malang.smartcity.R;
import com.kominfo.malang.smartcity._globalVariable.URLCollection;
import com.kominfo.malang.smartcity.internalLib.CheckConn;
import com.kominfo.malang.smartcity.internalLib.RetrofitLib.request_management.SendDataLogin;
import com.kominfo.malang.smartcity.internalLib.TextSafety;
import com.kominfo.malang.smartcity.internalLib.register.SecondRegisterData;

import java.text.SimpleDateFormat;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by USER on 26/03/2018.
 */

public class MainRegisterBioDetail_new extends FragmentActivity {

    String BASE_URL = URLCollection.DATA_SOURCE_LOKER;
    String TOKEN_MOBILE = "X00W";
    String status_warga, nik, TAG = "MainRegisterBioDetail_new";
    Intent intent;

    EditText eNama, eAlamat, eTmpLhr, eTglLhr;
    Button btnNext, btnPrev;
    Spinner spinnerJk, spinnerKwn;

    ProgressDialog progressDialog;

    Call<ResponseBody> getdata;

    SendDataLogin base_url_management;

    String message_galat = "inputkan data dengan benar",
            response_galat = "failed input data, coba lagi nanti";
    //safety text
    TextSafety textSafety = new TextSafety();
    CheckConn checkConn = new CheckConn();
    int mYear = 1994,
            mMonth = 03,
            mDay = 19;
    SecondRegisterData tempSecondRegData = new SecondRegisterData();
    //datepicker
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat simpleDateFormat;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_main_nomlg);

        spinnerJk = (Spinner) findViewById(R.id.spJk);
        ArrayAdapter<CharSequence> adapterJk = ArrayAdapter.createFromResource(this,
                R.array.item_jk, android.R.layout.simple_spinner_item);
        adapterJk.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerJk.setAdapter(adapterJk);

        spinnerKwn = (Spinner) findViewById(R.id.spKwn);
        ArrayAdapter<CharSequence> adapterKwn = ArrayAdapter.createFromResource(this,
                R.array.item_kwn, android.R.layout.simple_spinner_item);
        adapterKwn.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerKwn.setAdapter(adapterKwn);

        eNama = (EditText) findViewById(R.id.etNama);
        eAlamat = (EditText) findViewById(R.id.etAlamat);
        eTmpLhr = (EditText) findViewById(R.id.etTmpLhr);
        eTglLhr = (EditText) findViewById(R.id.etDate);

        btnNext = (Button) findViewById(R.id.btnNext);
        btnPrev = (Button) findViewById(R.id.btnPrev);

        intent = getIntent();
        nik = intent.getStringExtra("nik");

        Log.e(TAG, "onCreate: " + nik);

        //default tanggal
        eTglLhr.setText(String.valueOf(mYear) + "/" + String.valueOf(mMonth) + "/" + String.valueOf(mDay));

        String[] dataSession = tempSecondRegData.SecondRegisterGet(this);
        if (dataSession != null) {
            eNama.setText(dataSession[0]);
            eAlamat.setText(dataSession[1]);
            eTmpLhr.setText(dataSession[2]);
            eTglLhr.setText(dataSession[3]);
            spinnerJk.setSelection(Integer.valueOf(dataSession[4]));
            spinnerKwn.setSelection(Integer.valueOf(dataSession[5]));
        }

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e(TAG, "onClick: " + nik + "--"
                        + spinnerJk.getSelectedItemId() + "--"
                        + spinnerKwn.getSelectedItemId() + "--"
                        + eNama.getText().toString() + "--"
                        + eAlamat.getText().toString() + "--"
                        + eTglLhr.getText().toString() + "--"
                        + eTmpLhr.getText().toString() + "--"
                        + TOKEN_MOBILE + "--"

                );

                if (checkNullAble()) {
                    Log.e(TAG, "onClick: run");
                    tempSecondRegData.SecondRegisterSet(MainRegisterBioDetail_new.this,
                            eNama.getText().toString(),
                            eAlamat.getText().toString(),
                            eTmpLhr.getText().toString(),
                            eTglLhr.getText().toString(),
                            spinnerJk.getSelectedItemId(),
                            spinnerKwn.getSelectedItemId()
                    );

                    intent = new Intent(MainRegisterBioDetail_new.this, MainRegisterFixed.class);
                    startActivity(intent);
                    finish();

                } else {
                    Toast.makeText(MainRegisterBioDetail_new.this,
                            message_galat, Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(MainRegisterBioDetail_new.this, MainRegisterBio_new.class);
                startActivity(intent);
            }
        });

        eTglLhr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDateTimeField();
                datePickerDialog.show();
            }
        });
    }

    private void setDateTimeField() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH) + 1;
            mDay = c.get(Calendar.DAY_OF_MONTH);
        }
        datePickerDialog = new DatePickerDialog(MainRegisterBioDetail_new.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                eTglLhr.setText(String.valueOf(year) + "/" + String.valueOf(month) + "/" + String.valueOf(dayOfMonth));
            }
        }, mYear, mMonth, mDay);
    }

    private Boolean checkNullAble() {
        if (eNama.getText().toString() != null &&
                eAlamat.getText().toString() != null &&
                eTglLhr.getText().toString() != null &&
                eTmpLhr.getText().toString() != null) {

            return true;
        }
        return false;
    }


}