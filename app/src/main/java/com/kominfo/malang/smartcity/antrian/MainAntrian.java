package com.kominfo.malang.smartcity.antrian;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

import com.kominfo.malang.smartcity.R;

public class MainAntrian extends Activity implements View.OnClickListener{

    CardView a_kependudukan, a_kesehatan, a_perijinan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_antrian);

        a_kependudukan = (CardView) findViewById(R.id.a_kependudukan);
        a_kesehatan = (CardView) findViewById(R.id.a_kesehatan);
        a_perijinan = (CardView) findViewById(R.id.a_perijinan);

        a_kependudukan.setOnClickListener(this);
        a_kesehatan.setOnClickListener(this);
        a_perijinan.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent i ;

        switch (v.getId()){
            case R.id.a_kependudukan : i = new Intent(this, AntrianKe.class);startActivity(i); break;
            default: break;
        }
        switch (v.getId()){
        case R.id.a_kesehatan : i = new Intent(this, AntrianK.class);startActivity(i); break;
        default: break;
    }
        switch (v.getId()){
            case R.id.a_perijinan : i = new Intent(this, AntrianPe.class);startActivity(i); break;
    default: break;
}
    }
}
