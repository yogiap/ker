package com.kominfo.malang.smartcity.berita;

/**
 * Created by USER on 20/02/2018.
 */

public class Item {
    private String title;
    private String image;
    private String description;
    private String readmore;
    private String link;


    public Item(String title, String image, String description, String readmore, String link) {
        this.title = title;
        this.image = image;
        this.description = description;
        this.readmore = readmore;
        this.link = link;
    }


    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }


    public String getReadmore() {
        return readmore;
    }

    public void setReadmore(String readmore) {
        this.readmore = readmore;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}



