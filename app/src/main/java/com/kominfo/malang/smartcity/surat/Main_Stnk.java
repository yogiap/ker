package com.kominfo.malang.smartcity.surat;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

import com.kominfo.malang.smartcity.R;

public class Main_Stnk extends Activity {

    TextView textView;
    Typeface customFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stnk);

        customFont = Typeface.createFromAsset(getAssets(), "font/MontserratBold.ttf");
        textView = findViewById(R.id.textViewstnk);
        textView.setTypeface(customFont);

    }
}
