package com.kominfo.malang.smartcity;

import android.net.http.SslError;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class Sambat extends AppCompatActivity {

    WebView webV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sambat);

        webV = (WebView) findViewById(R.id.webV);
        String url = "https://sambat.malangkota.go.id";
        webV.setInitialScale(1);
        webV.getSettings().setUseWideViewPort(true);
        webV.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (!url.equals("about:blank")) {
                    view.loadUrl(url);
                }
                return false;
            }
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
            }
        });

        webV.loadUrl(url);

    }

    @Override
    public void onBackPressed() {
        if (webV.canGoBack()) {
            webV.goBack();
        }else {
            super.onBackPressed();
        }
    }

}