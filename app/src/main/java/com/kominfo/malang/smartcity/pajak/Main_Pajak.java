package com.kominfo.malang.smartcity.pajak;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import com.kominfo.malang.smartcity.cek_tagihan.CekTagihanPDAM;
import com.kominfo.malang.smartcity.R;
import com.kominfo.malang.smartcity.cek_tagihan.CekTagihanPbb;

public class Main_Pajak extends Activity {

    CardView cv_photel, cv_prestoran, cv_phiburan, cv_preklame, cv_pjalan, cv_pparkir, cv_pairtanah, cv_bphtb, cv_bumibangunan, cv_pdam, cv_pbb;
    TextView textView;
    Typeface customFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pajak);

        customFont = Typeface.createFromAsset(getAssets(), "font/MontserratBold.ttf");
        textView = findViewById(R.id.textViewpajak);
        textView.setTypeface(customFont);

        cv_photel = findViewById(R.id.cv_photel);
        cv_prestoran = findViewById(R.id.cv_prestoran);
        cv_phiburan = findViewById(R.id.cv_phiburan);
        cv_preklame = findViewById(R.id.cv_preklame);
        cv_pjalan = findViewById(R.id.cv_pjalan);
        cv_pparkir = findViewById(R.id.cv_pparkir);
        cv_pairtanah = findViewById(R.id.cv_pairtanah);
        cv_bphtb = findViewById(R.id.cv_bphtb);
        cv_bumibangunan = findViewById(R.id.cv_bumibangunan);
        cv_pdam = findViewById(R.id.cv_pdam);
        cv_pbb = findViewById(R.id.cv_pbb);

        cv_photel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Pajak.this, Pajak_Hotel.class);
                startActivity(i);
            }
        });

        cv_prestoran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Pajak.this, Pajak_Restoran.class);
                startActivity(i);
            }
        });

        cv_phiburan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Pajak.this, Pajak_Hiburan.class);
                startActivity(i);
            }
        });

        cv_preklame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Pajak.this, Pajak_Reklame.class);
                startActivity(i);
            }
        });

        cv_pjalan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Pajak.this, Pajak_Jalan.class);
                startActivity(i);
            }
        });

        cv_pparkir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Pajak.this, Pajak_Parkir.class);
                startActivity(i);
            }
        });

        cv_pairtanah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Pajak.this, Pajak_AirTanah.class);
                startActivity(i);
            }
        });

        cv_bphtb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Pajak.this, Pajak_Bphtb.class);
                startActivity(i);
            }
        });

        cv_bumibangunan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Pajak.this, Pajak_Pbb.class);
                startActivity(i);
            }
        });

        cv_pdam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Pajak.this, CekTagihanPDAM.class);
                startActivity(i);
            }
        });

        cv_pbb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main_Pajak.this, CekTagihanPbb.class);
                startActivity(i);
            }
        });

    }
}
