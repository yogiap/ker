package com.kominfo.malang.smartcity.info_layanan.kesehatan;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kominfo.malang.smartcity.R;

import java.util.List;

/**
 * Created by arimahardika on 06/03/2018.
 */

public class Adapter_Kesehatan extends RecyclerView.Adapter<Adapter_Kesehatan.KesehatanViewHolder> {

    private String TAG = "Adapter_Pendidikan";
    private List<Model_Kesehatan> listKesehatan;
    private Context context;

    public Adapter_Kesehatan(List<Model_Kesehatan> listKesehatan, Context context) {
        this.listKesehatan = listKesehatan;
        this.context = context;
    }

    @Override
    public KesehatanViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.kesehatan_list_row, parent, false);
        return new KesehatanViewHolder(view);
    }

    @Override
    public void onBindViewHolder(KesehatanViewHolder holder, int position) {
        final Model_Kesehatan model_kesehatan = listKesehatan.get(position);

        CharSequence getNama = model_kesehatan.getNama();
        CharSequence getJalan = model_kesehatan.getJalan();
        CharSequence getTelp = model_kesehatan.getTelp();

        holder.txt_nama.setText(getNama);
        holder.txt_jalan.setText(getJalan);
        holder.txt_telp.setText(getTelp);
        holder.gambar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("google.navigation:q=" + model_kesehatan.getJalan()));
                    //Uri.parse("http://maps.google.com/maps?saddr="+lat.toString()+","+lng.toString()+"&daddr="+jalan));
                    Log.e(TAG, "onClick: " + intent);
                    view.getContext().startActivity(intent);

                } catch (Exception e) {
                    Log.e(TAG, "onClick: " + e);
                }
            }
        });

        holder.txt_jalan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("google.navigation:q=" + model_kesehatan.getJalan()));
                    //Uri.parse("http://maps.google.com/maps?saddr="+lat.toString()+","+lng.toString()+"&daddr="+jalan));
                    Log.e(TAG, "onClick: " + intent);
                    view.getContext().startActivity(intent);

                } catch (Exception e) {
                    Log.e(TAG, "onClick: " + e);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listKesehatan.size();
    }

    public class KesehatanViewHolder extends RecyclerView.ViewHolder {

        TextView txt_nama, txt_jalan, txt_telp;
        ImageView gambar;
        LinearLayout linearLayout;

        public KesehatanViewHolder(View itemView) {
            super(itemView);

            txt_nama = itemView.findViewById(R.id.tv_kesehatan_nama);
            txt_jalan = itemView.findViewById(R.id.tv_kesehatan_alamat);
            txt_telp = itemView.findViewById(R.id.tv_kesehatan_noTelp);
            gambar = itemView.findViewById(R.id.img_kesehatan_gambar);

            linearLayout = itemView.findViewById(R.id.ll_kesehatan_linearLayout);
        }
    }
}
