package com.kominfo.malang.smartcity.internalLib;

/**
 * Created by USER on 01/03/2018.
 */

import android.util.Log;
import android.util.Patterns;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class TextSafety {

    String TAG = "TextSafety";

    public Boolean getValidEmail(String pattern, String matcher) {

        Pattern pattern1 = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher matcher1 = pattern1.matcher("suryahanggara@gmail.com");
        return matcher1.find();
    }

    public Boolean get_valid_email(String email) {
        Boolean valid_email = Patterns.EMAIL_ADDRESS.matcher(email).matches();
        Log.e(TAG, "onCreate: " + valid_email);
        return valid_email;
    }

    public Boolean get_valid_char(String string) {
        Pattern complete_ppt = Pattern.compile("[^\\p{Punct}]+?");
        Matcher matcher_com = complete_ppt.matcher(string);
        Log.e(TAG, "get_valid: matcher_complete: " + matcher_com.matches());
        return matcher_com.matches();
    }

    public Boolean get_valid_number(String string) {
        Pattern complete_ppt = Pattern.compile("(\\d+)");
        Matcher matcher_com = complete_ppt.matcher(string);
        Log.e(TAG, "get_valid: matcher_complete: " + matcher_com.matches());
        return matcher_com.matches();
    }
}
