package com.kominfo.malang.smartcity.main_feature;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.CardView;

import com.kominfo.malang.smartcity.PendidikanM;
import com.kominfo.malang.smartcity.R;
import com.kominfo.malang.smartcity._globalVariable.StaticVariable;
import com.kominfo.malang.smartcity.info_layanan.Main_Layanan_First_Level;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by lenovo on 2/5/2018.
 */

public class Layanan extends Activity {

    Intent i;

    Typeface customFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layanan);


        customFont = Typeface.createFromAsset(getAssets(), "font/myriad_pro_regular.otf");


        ButterKnife.bind(this);
    }

    @OnClick({R.id.pelayanan_kependudukan, R.id.pelayanan_pajak,
            R.id.pelayanan_perizinan, R.id.pelayanan_kesehatan, R.id.layanan_pendidikan})
    void onLayananClicked(CardView cardView) {
        switch (cardView.getId()) {
            case R.id.pelayanan_kependudukan:
                i = new Intent(this, Main_Layanan_First_Level.class);
                i.putExtra(StaticVariable.NAMA_LAYANAN, StaticVariable.KEPENDUDUKAN);
                this.startActivity(i);
                break;
            case R.id.pelayanan_pajak:
                i = new Intent(this, Main_Layanan_First_Level.class);
                i.putExtra(StaticVariable.NAMA_LAYANAN, StaticVariable.PAJAK);
                this.startActivity(i);
                break;
            case R.id.pelayanan_perizinan:
                i = new Intent(this, Main_Layanan_First_Level.class);
                i.putExtra(StaticVariable.NAMA_LAYANAN, StaticVariable.PERIZINAN);
                this.startActivity(i);
                break;
            case R.id.pelayanan_kesehatan:
                i = new Intent(this, Main_Layanan_First_Level.class);
                i.putExtra(StaticVariable.NAMA_LAYANAN, StaticVariable.KESEHATAN);
                this.startActivity(i);
                break;
            case R.id.layanan_pendidikan:
                i = new Intent(this, PendidikanM.class);
                this.startActivity(i);
                break;
        }
    }
}
