package com.kominfo.malang.smartcity.info_layanan.Utilities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kominfo.malang.smartcity.R;

import com.kominfo.malang.smartcity._globalVariable.StaticVariable;
import com.kominfo.malang.smartcity.info_layanan.Main_Layanan_Third_Level_Description;
import com.kominfo.malang.smartcity.info_layanan.kesehatan.Main_Kesehatan;
import com.kominfo.malang.smartcity.info_layanan.Main_Layanan_Second_Level;

/**
 * Created by arimahardika on 23/03/2018.
 */

public class Adapter_Layanan extends RecyclerView.Adapter<Adapter_Layanan.LayananViewHolder> {

    private CharSequence[] listLayanan;
    private Context context;

    public Adapter_Layanan(CharSequence[] listLayanan, Context context) {
        this.listLayanan = listLayanan;
        this.context = context;
    }

    @Override
    public LayananViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layanan_item_recycler, parent, false);
        return new LayananViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LayananViewHolder holder, final int position) {
        holder.tv_layanan.setText(listLayanan[position]);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CharSequence listLayananToString = listLayanan[position];
                getCategorySelected(listLayananToString);
            }
        });
    }

    private void getCategorySelected(CharSequence selected) {

        //==========================================================================================

        if (selected.equals(StaticVariable.DISPLAY_KARTU_KELUARGA)) {
            goToSecondLevel_LayananSublist(StaticVariable.KARTU_KELUARGA);

        } else if (selected.equals(StaticVariable.DISPLAY_AKTA)) {
            goToSecondLevel_LayananSublist(StaticVariable.AKTA);

        } else if (selected.equals(StaticVariable.DISPLAY_KTP)) {
            goToSecondLevel_LayananSublist(StaticVariable.KTP);

            //==========================================================================================

        } else if (selected.equals(StaticVariable.DISPLAY_IMB)) {
            goToThirdLevel_LayananDescription(StaticVariable.IMB);

        } else if (selected.equals(StaticVariable.DISPLAY_IZIN_GANGGUAN)) {
            goToThirdLevel_LayananDescription(StaticVariable.IZIN_GANGGUAN);

        } else if (selected.equals(StaticVariable.DISPLAY_IZIN_TONTONAN)) {
            goToThirdLevel_LayananDescription(StaticVariable.IZIN_TONTONAN);

        } else if (selected.equals(StaticVariable.DISPLAY_SIUP)) {
            goToThirdLevel_LayananDescription(StaticVariable.SIUP);

        } else if (selected.equals(StaticVariable.DISPLAY_IZIN_TOKO_MODERN)) {
            goToThirdLevel_LayananDescription(StaticVariable.IZIN_TOKO_MODERN);

            //==========================================================================================

        } else if (selected.equals(StaticVariable.DISPLAY_RUMAH_SAKIT)) {
            goToSecondLevel_LayananSublist(StaticVariable.RUMAH_SAKIT);

        } else if (selected.equals(StaticVariable.DISPLAY_PUSKESMAS_UTAMA)) {
            goToSecondLevel_LayananSublist(StaticVariable.PUSKESMAS_UTAMA);

        } else if (selected.equals(StaticVariable.DISPLAY_KLINIK)) {
            goToMainKesehatan(StaticVariable.KLINIK);

        } else if (selected.equals(StaticVariable.DISPLAY_APOTEK)) {
            goToMainKesehatan(StaticVariable.APOTEK);

        } else if (selected.equals(StaticVariable.DISPLAY_DOKTER_GIGI)) {
            goToMainKesehatan(StaticVariable.DOKTER_GIGI);

        } else if (selected.equals(StaticVariable.DISPLAY_LABORATORIUM)) {
            goToMainKesehatan(StaticVariable.LABORATORIUM);

        } else if (selected.equals(StaticVariable.DISPLAY_OPTIK)) {
            goToMainKesehatan(StaticVariable.OPTIK);

            //==========================================================================================

        } else if (selected.equals(StaticVariable.DISPLAY_PAJAK_DAERAH)) {
            goToThirdLevel_LayananDescription(StaticVariable.PAJAK_DAERAH);

        } else if (selected.equals(StaticVariable.DISPLAY_PAJAK_HOTEL)) {
            goToThirdLevel_LayananDescription(StaticVariable.PAJAK_HOTEL);

        } else if (selected.equals(StaticVariable.DISPLAY_PAJAK_RESTORAN)) {
            goToThirdLevel_LayananDescription(StaticVariable.PAJAK_RESTORAN);

        } else if (selected.equals(StaticVariable.DISPLAY_PAJAK_HIBURAN)) {
            goToThirdLevel_LayananDescription(StaticVariable.PAJAK_HIBURAN);

        } else if (selected.equals(StaticVariable.DISPLAY_PAJAK_REKLAME)) {
            goToThirdLevel_LayananDescription(StaticVariable.PAJAK_REKLAME);

        } else if (selected.equals(StaticVariable.DISPLAY_PAJAK_PENERANGAN_JALAN)) {
            goToThirdLevel_LayananDescription(StaticVariable.PAJAK_PENERANGAN_JALAN);

        } else if (selected.equals(StaticVariable.DISPLAY_PAJAK_PARKIR)) {
            goToThirdLevel_LayananDescription(StaticVariable.PAJAK_PARKIR);

        } else if (selected.equals(StaticVariable.DISPLAY_PAJAK_AIR_TANAH)) {
            goToThirdLevel_LayananDescription(StaticVariable.PAJAK_AIR_TANAH);

        } else if (selected.equals(StaticVariable.DISPLAY_BPHTB)) {
            goToThirdLevel_LayananDescription(StaticVariable.BPHTB);

        } else if (selected.equals(StaticVariable.DISPLAY_PAJAK_BUMI_BANGUNAN)) {
            goToThirdLevel_LayananDescription(StaticVariable.PAJAK_BUMI_BANGUNAN);

            //==========================================================================================
            //==========================================================================================
            //==========================================================================================

        } else if (selected.equals(StaticVariable.DISPLAY_KK_BARU)) {
            goToThirdLevel_LayananDescription(StaticVariable.KK_BARU);

        } else if (selected.equals(StaticVariable.DISPLAY_KK_HILANG_RUSAK)) {
            goToThirdLevel_LayananDescription(StaticVariable.KK_HILANG_RUSAK);

        } else if (selected.equals(StaticVariable.DISPLAY_KK_TAMBAH_ANGGOTA)) {
            goToThirdLevel_LayananDescription(StaticVariable.KK_TAMBAH_ANGGOTA);

        } else if (selected.equals(StaticVariable.DISPLAY_KK_PENGURANGAN_ANGGOTA)) {
            goToThirdLevel_LayananDescription(StaticVariable.KK_PENGURANGAN_ANGGOTA);

        } else if (selected.equals(StaticVariable.DISPLAY_KK_PERUBAHAN_ANGGOTA)) {
            goToThirdLevel_LayananDescription(StaticVariable.KK_PERUBAHAN_ANGGOTA);

            //==========================================================================================

        } else if (selected.equals(StaticVariable.DISPLAY_AKTA_KELAHIRAN)) {
            goToThirdLevel_LayananDescription(StaticVariable.AKTA_KELAHIRAN);

        } else if (selected.equals(StaticVariable.DISPLAY_AKTA_KEMATIAN)) {
            goToThirdLevel_LayananDescription(StaticVariable.AKTA_KEMATIAN);

        } else if (selected.equals(StaticVariable.DISPLAY_AKTA_PERKAWINAN)) {
            goToThirdLevel_LayananDescription(StaticVariable.AKTA_PERKAWINAN);

        } else if (selected.equals(StaticVariable.DISPLAY_PENCATATAN_PERUBAHAN_NAMA)) {
            goToThirdLevel_LayananDescription(StaticVariable.AKTA_PENCATATAN_PERUBAHAN_NAMA);

        } else if (selected.equals(StaticVariable.DISPLAY_PENCATATAN_PENGAKUAN_ANAK)) {
            goToThirdLevel_LayananDescription(StaticVariable.AKTA_PENCATATAN_PENGAKUAN_ANAK);

        } else if (selected.equals(StaticVariable.DISPLAY_KUTIPAN_KEDUA_AKTA_PENCATATAN_SIPIL)) {
            goToThirdLevel_LayananDescription(StaticVariable.KUTIPAN_KEDUA_AKTA_PENCATATAN_SIPIL);

        } else if (selected.equals(StaticVariable.DISPLAY_ADOPSI)) {
            goToThirdLevel_LayananDescription(StaticVariable.AKTA_ADOPSI);

        } else if (selected.equals(StaticVariable.DISPLAY_AKTA_PENGESAHAN_ANAK)) {
            goToThirdLevel_LayananDescription(StaticVariable.AKTA_PENGESAHAN_ANAK);

            //==========================================================================================

        } else if (selected.equals(StaticVariable.DISPLAY_KTP_BARU)) {
            goToThirdLevel_LayananDescription(StaticVariable.KTP_BARU);

        } else if (selected.equals(StaticVariable.DISPLAY_KTP_HILANG_RUSAK)) {
            goToThirdLevel_LayananDescription(StaticVariable.KTP_HILANG_RUSAK);

        } else if (selected.equals(StaticVariable.DISPLAY_KTP_SALAH_DIRUBAH)) {
            goToThirdLevel_LayananDescription(StaticVariable.KTP_SALAH_DIRUBAH);

            //==========================================================================================

        } else if (selected.equals(StaticVariable.DISPLAY_RUMAH_SAKIT_UMUM)) {
            goToMainKesehatan(StaticVariable.RUMAH_SAKIT_UMUM);

        } else if (selected.equals(StaticVariable.DISPLAY_RUMAH_SAKIT_IBU_DAN_ANAK)) {
            goToMainKesehatan(StaticVariable.RUMAH_SAKIT_IBU_DAN_ANAK);

        } else if (selected.equals(StaticVariable.DISPLAY_RUMAH_SAKIT_BERSALIN)) {
            goToMainKesehatan(StaticVariable.RUMAH_SAKIT_BERSALIN);

        } else if (selected.equals(StaticVariable.DISPLAY_PUSKESMAS)) {
            goToMainKesehatan(StaticVariable.PUSKESMAS);

        } else if (selected.equals(StaticVariable.DISPLAY_PUSKESMAS_UPT)) {
            goToMainKesehatan(StaticVariable.PUSKESMAS_UPT);

            //==========================================================================================

        } else if (selected.equals(StaticVariable.DISPLAY_INSTANSI)) {
            goToThirdLevel_LayananDescription(StaticVariable.INSTANSI);

        } else if (selected.equals(StaticVariable.DISPLAY_DINAS_KESEHATAN)) {
            goToThirdLevel_LayananDescription(StaticVariable.DINAS_KESEHATAN);

        } else if (selected.equals(StaticVariable.DISPLAY_PELAYANAN_PAJAK)) {
            goToSecondLevel_LayananSublist(StaticVariable.PELAYANAN_PAJAK);

        } else if (selected.equals(StaticVariable.DISPLAY_DINAS_PERIZINAN)) {
            goToSecondLevel_LayananSublist(StaticVariable.DINAS_PERIZINAN);
        }
    }

    private void goToThirdLevel_LayananDescription(String staticVariable) {
        Intent i;
        i = new Intent(context, Main_Layanan_Third_Level_Description.class);
        i.putExtra(StaticVariable.NAMA_LAYANAN, staticVariable);
        context.startActivity(i);
    }

    private void goToSecondLevel_LayananSublist(String staticVariable) {
        Intent i;
        i = new Intent(context, Main_Layanan_Second_Level.class);
        i.putExtra(StaticVariable.NAMA_LAYANAN, staticVariable);
        context.startActivity(i);
    }

    private void goToMainKesehatan(String staticVariable) {
        Intent i;
        i = new Intent(context, Main_Kesehatan.class);
        i.putExtra(StaticVariable.NAMA_LAYANAN, staticVariable);
        context.startActivity(i);
    }

    @Override
    public int getItemCount() {
        return listLayanan.length;
    }

    public class LayananViewHolder extends RecyclerView.ViewHolder {

        TextView tv_layanan;
        CardView cardView;
        TextView sumber_dispenduk;
        TextView sub_tittle;

        public LayananViewHolder(View itemView) {
            super(itemView);

            tv_layanan = itemView.findViewById(R.id.tv_layanan_recycler);
            cardView = itemView.findViewById(R.id.cardView_layanan);
            sumber_dispenduk = itemView.findViewById(R.id.sumber_dispenduk);
            sub_tittle = itemView.findViewById(R.id.sub_tittle);
            context = itemView.getContext();
        }
    }
}
