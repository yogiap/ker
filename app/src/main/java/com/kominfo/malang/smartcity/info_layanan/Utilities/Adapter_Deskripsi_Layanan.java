package com.kominfo.malang.smartcity.info_layanan.Utilities;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kominfo.malang.smartcity.R;

/**
 * Created by arimahardika on 05/04/2018.
 */

public class Adapter_Deskripsi_Layanan extends RecyclerView.Adapter<Adapter_Deskripsi_Layanan.DeskripsiViewHolder> {

    private CharSequence[] listLayanan;
    private int number;
    private Context context;

    public Adapter_Deskripsi_Layanan(CharSequence[] listLayanan, Context context) {
        this.listLayanan = listLayanan;
        this.context = context;
    }

    @Override
    public DeskripsiViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layanan_item_description, parent, false);
        return new DeskripsiViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DeskripsiViewHolder holder, int position) {
        holder.tv_nomor.setText(String.valueOf(position + 1));
        holder.tv_layanan.setText(listLayanan[position]);
    }

    @Override
    public int getItemCount() {
        return listLayanan.length;
    }

    public class DeskripsiViewHolder extends RecyclerView.ViewHolder {

        TextView tv_layanan, tv_nomor;
        CardView cardViewNomer, cardViewLayanan;

        public DeskripsiViewHolder(View itemView) {
            super(itemView);

            tv_layanan = itemView.findViewById(R.id.tv_layanan_recycler_description);
            tv_nomor = itemView.findViewById(R.id.tv_nomor);

            cardViewNomer = itemView.findViewById(R.id.cardView_nomor);
            cardViewLayanan = itemView.findViewById(R.id.cardView_layanan_description);
        }
    }
}
