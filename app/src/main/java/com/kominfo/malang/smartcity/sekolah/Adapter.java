package com.kominfo.malang.smartcity.sekolah;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kominfo.malang.smartcity.R;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    private Context context;
    private List<Model> list;

    public Adapter(Context context, List<Model> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.list_sekolah, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Model mo = list.get(position);

        holder.textNo.setText(mo.getNo());
        holder.textNpsn.setText(String.valueOf(mo.getNpsn()));
        holder.textNama.setText(String.valueOf(mo.getNama()));
        holder.textAlamat.setText(String.valueOf(mo.getAlamat()));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textNo, textNpsn, textNama, textAlamat;

        public ViewHolder(View itemView) {
            super(itemView);

            textNo= itemView.findViewById(R.id.main_no);
            textNpsn = itemView.findViewById(R.id.main_npsn);
            textNama = itemView.findViewById(R.id.main_nama);
            textAlamat = itemView.findViewById(R.id.main_alamat);
        }
    }
}
